#ifndef MJUNIK_PRIORITY_QUEUE_HPP
#define MJUNIK_PRIORITY_QUEUE_HPP

#include <functional>
#include <queue>
#include <utility>

#include "edge.hpp"

namespace mjunik {

template <typename WeightType, typename KeyType>
class priority_queue {

private:
	/* Contains pairs {weight, vertex} */
	using edge_t = edge<WeightType, KeyType>;
	using queue_t = std::priority_queue<edge_t, std::vector<edge_t>, std::greater<edge_t>>;

	queue_t queue;
	const unsigned int max_size;

	/* A hack to access the priority_queue underlying container. */
	/* Source: https://stackoverflow.com/questions/1185252/ */
	std::vector<edge_t>& underlying(queue_t& q) {
			struct hacked_queue : private queue_t {
				static std::vector<edge_t>& underlying(queue_t& q) {
					return q.*&hacked_queue::c;
				}
			};
		return hacked_queue::underlying(q);
	}

public:
	priority_queue(unsigned int p_max_size) : max_size(p_max_size) {}

	std::pair<edge_t, bool> top() {
		if(queue.size() == max_size) {
			return std::make_pair(queue.top(), true);
		} else {
			return std::make_pair(edge_t(0, 0, 0), false);
		}
	}

	void push(WeightType p_weight, KeyType p_vertex, KeyType p_vertex_old) {
		if(queue.size() == max_size) {
			queue.pop();
		}

		queue.push(edge_t(p_weight, p_vertex, p_vertex_old));
		//std::cout << "Added to queue. " << queue.size() << std::endl;
	}

	WeightType getTotalWeight() {
		WeightType total = 0;
		int size = queue.size();
		std::vector<edge_t> v = underlying(queue);
		while(size-- > 0) {
			total += v[size].weight;
		}
		return total;
	}
};

}

#endif