#ifndef MJUNIK_EDGE_HPP
#define MJUNIK_EDGE_HPP

namespace mjunik {

template <typename Weight, typename Key>
class edge {
private:
    Key old_key;
public:
    Key to;
    Weight weight;

    edge(Weight p_weight, Key p_to, Key p_old_key) : weight(p_weight), to(p_to), old_key(p_old_key) {}
    edge(const edge<Weight,Key>& other) : weight(other.weight), to(other.to), old_key(other.old_key) {}
    edge(edge<Weight,Key>&& other) : weight(other.weight), to(other.to), old_key(other.old_key) {}

    edge<Weight, Key>& operator=(const edge<Weight,Key> &other) {
        old_key = other.old_key;
        to = other.to;
        weight = other.weight;
        return *this;
    }

    edge<Weight, Key>& operator=(edge<Weight, Key> &&other) {
        old_key = other.old_key;
        to = other.to;
        weight = other.weight;
        return *this;
    }

    bool operator==(const edge<Weight,Key> &other) const {
        return weight == other.weight && old_key == other.old_key;
    }

    bool operator<(const edge<Weight,Key> &other) const {
        return weight < other.weight || (weight == other.weight && old_key < other.old_key);
    }

    bool operator>(const edge<Weight,Key> &other) const {
        return weight > other.weight || (weight == other.weight && old_key > other.old_key);
    }
};

}

#endif