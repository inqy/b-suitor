#ifndef MJUNIK_GRAPH_HPP
#define MJUNIK_GRAPH_HPP

#include <algorithm>
#include <fstream>
#include <iostream>
#include <list>
#include <string>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "atomic_wrap.hpp"
#include "blimit.hpp"
#include "edge.hpp"
#include "priority_queue.hpp"

namespace mjunik {

class error_loading_file {};

template <typename Weight, typename Key, typename BType>
class graph {
public:
    using edge_t = edge<Weight, Key>;
    using adjacency_list = std::vector<std::vector<edge_t>>;
    using suitor_list = std::vector<priority_queue<Weight, Key>>;
    using b_vector = std::vector<std::vector<BType>>;
    using converter_vector = std::vector<Key>;
    using inverter_map = std::unordered_map<Key, Key>;
    using iterator_vector = std::vector<typename std::vector<edge_t>::iterator>;

    adjacency_list neighbors;
    suitor_list suitors;
    b_vector bvalues;
    converter_vector converter;
    inverter_map inverter;
    iterator_vector iters;

    int b_index = 0;

    void loadNeighborsFromFile(std::string p_input_filename) {
        std::ifstream input_file{p_input_filename};
        Key index = 0;
        for (std::string line; std::getline(input_file, line);)
        {
            char c = line.at(0);
            if(c != '#') {
                std::istringstream iss(line);
                Key v1, v2;
                Weight w;
                if (!(iss >> v1 >> v2 >> w)) { 
                    throw error_loading_file{};
                }

                Key new_v1;
                Key new_v2;

                if(inverter.find(v1) == inverter.end()) {
                    inverter.emplace(v1, index);
                    converter.push_back(v1);
                    neighbors.emplace_back();
                    new_v1 = index;
                    index++;
                } else {
                    new_v1 = inverter[v1];
                }

                if(inverter.find(v2) == inverter.end()) {
                    inverter.emplace(v2, index);
                    converter.push_back(v2);
                    neighbors.emplace_back();
                    new_v2 = index;
                    index++;
                } else {
                    new_v2 = inverter[v2];
                }

                neighbors[new_v1].push_back(edge_t(w, new_v2, v2));
                neighbors[new_v2].push_back(edge_t(w, new_v1, v1));
            }
        }
    }

    void generateBValues(BType p_blimit) {
        for(int i = 0; i <= p_blimit; i++) {
            bvalues.emplace_back();
        }

        for(Key i = 0; i < neighbors.size(); i++) {
            Key key = converter[i];

            int max = 0;
            for(int j = 0; j <= p_blimit; j++) {
                bvalues[j].push_back(bvalue(j, key));
            }
        }
    }

    void sortNeighbors() {
        for(Key key = 0; key < neighbors.size(); key++) {
            auto& v = neighbors[key];
            std::sort(v.begin(), v.end(), std::greater<edge_t>());
        }
    }

    void generateSuitorQueues() {
        //iters.clear();
        for(Key key = 0; key < neighbors.size(); key++) {
            suitors.emplace_back(bvalues[b_index][key]);
            iters.push_back(neighbors[key].begin());
        }
    }

    std::vector<Key> generateQueue() {
        std::vector<Key> queue;
        for(Key key = 0; key < neighbors.size(); key++) {
            queue.push_back(key);
        }

        return queue;
    }

    std::pair<edge_t, bool> argMax(Key u, std::vector<atomic_wrap>* locks) {
        auto& it = iters[u];

        while(it != neighbors[u].end()) {
            auto neighbor = (*it);
            int neighbor_key = neighbor.to;
            int neighbor_weight = neighbor.weight;

            if(bvalues[b_index][neighbor_key] > 0) {
                while((*locks)[neighbor_key].v.test_and_set());
                std::pair<edge_t, bool> neighbor_last_obj = suitors.at(neighbor_key).top();
                (*locks)[neighbor_key].v.clear();
                edge_t neighbor_last = neighbor_last_obj.first;
                bool neighbor_last_exists = neighbor_last_obj.second;
                
                if(!neighbor_last_exists || edge_t(neighbor_weight, u, converter[u]) > neighbor_last) {
                    it++;
                    return std::make_pair(neighbor, true);
                }
            }
            it++;
        }

        return std::make_pair(edge_t(0, 0, 0), false);
    }

    bool stillViable(Key u, Key v, Weight w) {
        std::pair<edge_t, bool> neighbor_last_obj = suitors.at(v).top();
        edge_t neighbor_last = neighbor_last_obj.first;
        bool neighbor_last_exists = neighbor_last_obj.second;
        if(!neighbor_last_exists || edge_t(w, u, converter[u]) > neighbor_last) {
            return true;
        }

        return false;
    } 

    /* This pops all the elements from all suitor queues!! */
    Weight getMatchingValue() {
        Weight total = 0;
        for(Key key = 0; key < neighbors.size(); key++) {
            auto& q = suitors[key];
            total += q.getTotalWeight();;
        }
        return total;
    }

    void cleanup() {
        suitors = suitor_list();
        iters = iterator_vector();
    }
};

}

#endif