#ifndef MJUNIK_ATOMIC_HPP
#define MJUNIK_ATOMIC_HPP

#include <atomic>

namespace mjunik {
	
class atomic_wrap {
public:
	std::atomic_flag v;

	atomic_wrap() : v(false) {}
	atomic_wrap(const atomic_wrap& other) : v(false) {}
};

}

#endif