#include "graph.hpp"
#include "atomic_wrap.hpp"

#include <algorithm>
#include <iostream>
#include <list>
#include <memory>
#include <mutex>
#include <utility>
#include <string>
#include <thread>

class adorate {
private:
    using b_graph = mjunik::graph<int, int, unsigned int>;
    using atomic = mjunik::atomic_wrap;
    using queue_t = std::vector<int>;

    int thread_count;
    std::string input_filename;
    int b_limit;

    b_graph g;
    queue_t queue;
    queue_t next_queue;
    std::vector<atomic> locks;
    std::vector<std::vector<unsigned int>> bmask;
    std::mutex queue_protection;

    void match(int bindex, queue_t::iterator begin, queue_t::iterator end) {
        queue_t next_queue_local;
        std::vector<int> bmask_local;
        for(int i = 0; i < g.neighbors.size(); i++) {
            bmask_local.push_back(0);
        }

        for(;begin != end;begin++) {
            int u = (*begin);

            while(bmask[bindex][u]+bmask_local[u] < g.bvalues[bindex][u]) {
                const auto& arg_max = g.argMax(u, &locks);
                bool found = arg_max.second;

                if(!found) {
                    break;
                } else {
                    int max_weight = arg_max.first.weight;
                    int key = arg_max.first.to;

                    while(locks[key].v.test_and_set());
                    auto y = g.suitors.at(key).top();
                    if(g.stillViable(u, key, max_weight)) {
                        g.suitors.at(key).push(max_weight, u, g.converter[u]);
                    } else {
                        locks[key].v.clear();
                        continue;
                    }
                    locks[key].v.clear();

                    bmask_local[u]++;

                    if(y.second) {
                        int y_key = y.first.to;
                        bmask_local[y_key]--;
                        next_queue_local.push_back(y_key);
                    }
                }
            }
        }

        {
            std::lock_guard<std::mutex> lock(queue_protection);
            for(int j = 0; j < bmask_local.size(); j++) {
                bmask[bindex][j] += bmask_local[j];
            }
            next_queue.insert(next_queue.end(), next_queue_local.begin(), next_queue_local.end());
        }
    }

    void prepareBMask() {
        bmask.clear();
        for(int i = 0; i < g.bvalues.size(); i++) {
            std::vector<unsigned int> v;
            bmask.push_back(v);
            for(int j = 0; j < g.bvalues[i].size(); j++) {
                bmask[i].push_back(0);
            }
        }
    }

    void run() {
        for(int i = 0; i < g.neighbors.size(); i++) {
            locks.emplace_back();
        }

        for(int i = 0; i <= b_limit; i++) {
                g.generateSuitorQueues();
                queue = g.generateQueue();

                while(!queue.empty()) {
                    int remaining_length = queue.size();
                    queue_t::iterator cur_end = queue.begin();

                    std::vector<std::thread> threads;

                    while(remaining_length > 0) {
                        int min_candidate = 4096 > (queue.size()/thread_count)+1 ? 4096 : (queue.size()/thread_count)+1;
                        int frag_len = remaining_length < min_candidate ? remaining_length : min_candidate;
                        //std::cout << "Starting thread for " << frag_len << "/" << queue.size() << std::endl;

                        queue_t::iterator old_end = cur_end;
                        remaining_length -= frag_len;
                        cur_end = cur_end+frag_len;

                        if(remaining_length > 0) {
                             std::thread t{[i, this, old_end, frag_len]{match(i, old_end, old_end+frag_len);}};
                             threads.push_back(std::move(t));
                        } else {
                             match(i, old_end, old_end+frag_len);
                        }
                    }

                    for(std::thread& t : threads) {
                        t.join();
                    }

                    //std::swap(queue, next_queue);
                    //next_queue.clear();
                    std::sort(next_queue.begin(), next_queue.end(), std::greater<unsigned int>());
                    queue.clear();
                    queue.insert(queue.end(), next_queue.begin(), std::unique(next_queue.begin(), next_queue.end()));
                    next_queue.clear();
                }

                std::cout << g.getMatchingValue()/2 << std::endl;
                g.cleanup();
                g.b_index++;
        }
    }

public:

    adorate(int p_count, std::string p_input, int p_limit) : thread_count(p_count), 
        input_filename(p_input), b_limit(p_limit) {
        g.loadNeighborsFromFile(input_filename);
        if(p_limit >= 0) {
            g.generateBValues(b_limit);
            g.sortNeighbors();
            prepareBMask(); 
            run();
        }
    }

};


int main(int argc, char* argv[]) {
    if (argc != 4) {
        std::cerr << "usage: "<<argv[0]<<" thread-count inputfile b-limit"<< std::endl;
        return 1;
    }

    int thread_count = std::stoi(argv[1]);
    std::string input_filename{argv[2]};
    int b_limit = std::stoi(argv[3]);

    adorate obj{thread_count, input_filename, b_limit};
}