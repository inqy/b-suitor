# Overview
This is a C++ implementation of the parallel b-Suitor algorithm created for the Parallel Programming course at the University of Warsaw.  
The paper describing the algorithm can be found [here](https://www.cs.purdue.edu/homes/apothen/Papers/bMatching-SISC-2016.pdf)  
A short description of the algorithm copied from the paper:
>We describe a half-approximation algorithm, b-Suitor, for computing a b-Matching
of maximum weight in a graph with weights on the edges. b-Matching is a generalization of the
well-known Matching problem in graphs, where the objective is to choose a subset of M edges in
the graph such that at most a specified number b(v) of edges in M are incident on each vertex v.
Subject to this restriction we maximize the sum of the weights of the edges in M. W

# Usage
The program can be executed with  
```
./adorate n-threads graph-path b-limit
```  
where b-limit is the number of different b(v) functions you want to run the algorithm with.  
You can define the b(v) function by providing an implementation of this method from [blimit.hpp](../mj385655/blimit.hpp):  
```
unsigned int bvalue(unsigned int method, unsigned long node_id);
```  

# Testing script
There is a bash testing script provided [here](test.sh). The script, for each test found in the `data` directory compiles the program with an appropiate implementation of the blimit method found in `libs`, executes it, and compares the results with the files in `ref-results`. The .in/.cpp/.out files are automatically matched by file names.  
The supported flags are:  
- `-t/--test $testname` to run only a single test  
- `-m/--measure` to measure the running time of the program (*including* loading the graph)  
- `-n/--threads $n` to set the maximum number of threads
 
The contents of the input files have to be in format `vertice1 vertice2 weight` with a `# blimit n` line denoting the blimit for the test. Example:

```
# blimit 1
# this graph is adopted from Khan et al, 2016
0 2 8
0 3 6
0 4 4
0 5 2
1 2 3
1 3 7
1 4 1
1 5 9
```
and an example working directory looks like this:
```
>b-suitor
	>test.sh
	>mj385655
		>adorate.cpp
		>...
		>build (created automatically)
			>adorate-testname (created automatically)
			>...
	>lib
		>blimit-testname.hpp
		>...
	>data
		>testname.in
		>...
	>ref-results
		>testname.out
		>...
```